#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
class Stos
{
    public:
        int *tab;
        int wierzch=-1,rozmiar;
        Stos():rozmiar(10)
        {
            tab=new int[10];
            cout<<"Stos::Stos() "<<this<<endl;
        }
        ~Stos()
        {
            cout<<"Stos::~Stos() "<<this<<endl;
            delete tab;
        }
        void pop()
        {
            this->wierzch--;
        }
        void push(int l)
        {
            tab[++wierzch]=l;
        }
        bool empty()
        {
            return wierzch==-1;
        }
        int top()
        {
            return tab[wierzch];
        }
        Stos* adres()
        {
            return this;
        }
};
void f(Stos ss,int a)
{
    ss.push(a);
}
int main()
{
    Stos s;
    s.push(0);
    f(s,1);
    f(s,2);
    while(!s.empty())
    {
        cout<<
        s.top();
        s.pop();
    }
    return 0;
}
